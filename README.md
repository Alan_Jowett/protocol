OpenConnect Protocol Documentation
==================================

This project is about documenting the protocol used by the [Openconnect VPN](https://gitlab.com/openconnect/ocserv) client and server.

 - [Generated document from this repository](https://openconnect.gitlab.io/protocol/)
 - Current released document from IETF web site: [The OpenConnect VPN Protocol Version 1.1](https://tools.ietf.org/id/draft-mavrogiannopoulos-openconnect-02.html)
 - [XML source file is also available from IETF](https://tools.ietf.org/id/draft-mavrogiannopoulos-openconnect-02.xml)
